import React from 'react';
import logo from './logo.svg';
import './AppLayout.css';
import './AppStyle.css';

import ROSLIB from 'roslib';

import { 
  FaBatteryEmpty, 
  FaBatteryHalf,
  FaBatteryThreeQuarters, 
  FaBatteryFull, 
  FaCheck 
} from 'react-icons/fa';




const DEBUG = true;


class BackendMock {

  constructor() {
    this.events = {
      batteries: [],
      moving: []
    }

    this.moving = true;
    setInterval(() => {
      this.events.moving.forEach(value => value(this.moving));
      this.moving = !this.moving;
    }, 2000);
  }

  connect() {
    return new Promise((resolve, reject) => {
    })
  }

  on(event, callback) {
    this.events[event].push(callback);
  }
}

class BackendImpl {

}

const Backend = DEBUG ? BackendMock : BackendImpl;


const Batteries = function (props) {
  const icon = 
    props.level <= 75 ?
      props.level <= 50 ?
        props.level <= 10 ?
          <FaBatteryEmpty className="battery-icon" /> :
        <FaBatteryHalf className="battery-icon" /> :
      <FaBatteryThreeQuarters className="battery-icon"/> :
    <FaBatteryFull className="battery-icon" />

  return icon ;
}


const Material = function(props) {

  let className = "material";
  if (!props.collectable) 
    className += " material-uncollectable";

  return (
    <div className={ className } >
      <div className={`material-name ${ props.done ? 'material-done' : ""}`} >
        {props.name}
        { props.done ? <FaCheck /> : null }
      </div>

      <div 
      className={`material-description ${props.description ? 'material-description-expanded' : 'material-description-collapsed'}`} 
    >{props.description} {props.name} </div>

    </div>
  )
}


const description = {
  MOVING_TO: "A caminho do material ",
  WAITING_AT: "Á espera junto a ",
  DELIVERING: "A entregar ",
  PICKUP: "Á espera de recolha de ",
  EMPTY: "",
}

class App extends React.Component {



  constructor(props) {
    super(props);
    this.state = {
      connected: false,
      batteries: 90,
      order: {
        id: 329816,
        description: "VIEROQUARTZ PRIM. BR. TINTÁVEL",  
        materials: [
          {
            name: "M1",
            collectable: true,
            description: null,
            done: false
          },

          {
            name: "M2",
            collectable: false,
            description: null,
            done: false
          },
          {
            name: "M3",
            collectable: true,
            description: null,
            done: false
          },
          {
            name: "M1",
            collectable: true,
            description: null,
            done: false
          }
        ],
      }
    }

    this.backend = null;
    
  }

  componentDidMount() {

    const backend = new Backend()
    backend.connect()
      .then(this.setState({ connected: true }))

    backend.on("batteries", level => {
      this.setState({ batteries: level })
    })

    backend.on("moving", this.on_moving.bind(this));
    this.backend = backend;
  }

  on_moving(is_moving) {
    console.log(is_moving ? "moving" : "not moving")
    let order = JSON.parse(JSON.stringify(this.state.order));
    let materials = order.materials;
    
    for (let i = 0; i < materials.length; i++) {
      if (materials[i].done) continue;
      if (!materials[i].collectable) continue;
      
      switch(materials[i].description) {
        case description.MOVING_TO:
          if (!is_moving) materials[i].description = description.WAITING_AT
          break;
        case description.WAITING_AT:
          if (is_moving) materials[i].description = description.DELIVERING
          break;
        case description.DELIVERING:
          if(!is_moving) materials[i].description = description.PICKUP
          break;
        case description.PICKUP:
          if (is_moving) {
            materials[i].done = true;
            materials[i].description = description.EMPTY;
            for (let j = i+1; j < materials.length; j++)
              if (materials[j].collectable) {
                materials[j].description = description.MOVING_TO;
                break;
              }
          }
          break;

        default:
          if (is_moving) materials[i].description = description.MOVING_TO;   
          break;
        }
          
      order.materials = materials;
      this.setState({ order: order });
      return;
    }
    console.log("order complete");
  }


  render() {
    const { connected, batteries, order } = this.state;
    return (
      <div className="App">
        <div className="header" >
          <p>ARCO</p>
          <Batteries level={batteries} />
        </div>

        <div className="main" >
          <p className="order-name" >ORDEM {order.id}</p>
          <p className="order-description" >{order.description}</p>

          <div className="order" >

            { order.materials.map(el => ( 
              <Material
                key={order.materials.indexOf(el)}
                name={ el.name }
                collectable={ el.collectable }
                description={ el.description } 
                done={ el.done }
              />
            ))}
          
          </div>
        </div>
  
      </div>
    );
  }
}

export default App;
