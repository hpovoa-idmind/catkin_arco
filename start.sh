#! /bin/bash

source /opt/ros/kinetic/setup.bash
source /home/arco/catkin_arco/devel/setup.bash
export ROS_MASTER_URI=http://localhost:11311
export ROS_HOSTNAME=arco
roslaunch idmind_launch robot.launch
