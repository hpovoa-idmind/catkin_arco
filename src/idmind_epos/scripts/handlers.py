
#!/usr/bin/env python


import math

import rospy
from geometry_msgs.msg import Twist
from idmind_msgs.msg import Leds as LedsMsg

from teleop import JoyHandler


NLEDS = 8


class Move(JoyHandler):

    def __init__(self):
        super(Move, self).__init__()

        self.enabled = False

        self.x_speed = 1.0  # 1 seconds for 1 meter
        self.y_speed = 1.0
        self.w_speed = 1.57   # 4 seconds for a full turn

        self.twist = Twist()

        self.pub = rospy.Publisher("idmind_motors/twist", Twist, queue_size=1)
        self.leds_pub = rospy.Publisher("idmind_sensorsboard/leds", LedsMsg, queue_size=1)

        self.on_click("start", self.toggle_enable)
        self.on_change("left_knob", self.update_linear)
        self.on_change("right_knob", self.update_angular)

    def toggle_enable(self):
        self.enabled = not self.enabled
        color = 255 if self.enabled else 0
        msg = LedsMsg()
        msg.red = [color] * NLEDS
        msg.green = [color] * NLEDS
        msg.blue = [color] * NLEDS
        self.leds_pub.publish(msg)
        rospy.loginfo("teleop enabled" if self.enabled else "teleop disabled")

    def update_linear(self, val):
        x, y = val
        self.twist.linear.x = y * self.x_speed
        self.twist.linear.y = x * self.y_speed

    def update_angular(self, val):
        x, _ = val
        self.twist.angular.z = x * self.w_speed

    def run(self):
        if self.enabled:
            self.pub.publish(self.twist)


class Leds(JoyHandler):

    def __init__(self):
        super(Leds, self).__init__()

        self.pub = rospy.Publisher("idmind_sensorsboard/leds", LedsMsg, queue_size=1)

        self.on_click("a", self.send_green)
        self.on_click("b", self.send_red)
        self.on_click("x", self.send_blue)

    def send_red(self):
        msg = LedsMsg()
        msg.red = [255] * NLEDS
        msg.green = [0] * NLEDS
        msg.blue = [0] * NLEDS
        self.pub.publish(msg)

    def send_green(self):
        msg = LedsMsg()
        msg.red = [0] * NLEDS
        msg.green = [255] * NLEDS
        msg.blue = [0] * NLEDS
        self.pub.publish(msg)

    def send_blue(self):
        msg = LedsMsg()
        msg.red = [0] * NLEDS
        msg.green = [0] * NLEDS
        msg.blue = [255] * NLEDS
        self.pub.publish(msg)

    def run(self):
        pass


