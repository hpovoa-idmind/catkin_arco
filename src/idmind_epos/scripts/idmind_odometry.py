#! usr/bin/env python


import math
import rospy
from idmind_epos.msg import Encoders
from nav_msgs.msg import Odometry
import tf


WHEEL_DISTANCE = 0.4
LEFT_TICKS_PER_TURN = 1000
RIGHT_TICKS_PER_TURN = -1000
OVERFLOW_THRESHOLD = 3000
POSITION_COVARIANCE = [
    .0001, .0000, .0000, .0000, .0000, .0000,
    .0000, .0002, .0000, .0000, .0000, .0000,
    .0000, .0000, .0002, .0000, .0000, .0000,
    .0000, .0000, .0000, .0002, .0000, .0000,
    .0000, .0000, .0000, .0000, .0002, .0000,
    .0000, .0000, .0000, .0000, .0000, .0004,
]
VELOCITY_COVARIANCE = [
    .0001, .0000, .0000, .0000, .0000, .0000,
    .0000, .0002, .0000, .0000, .0000, .0000,
    .0000, .0000, .0002, .0000, .0000, .0000,
    .0000, .0000, .0000, .0002, .0000, .0000,
    .0000, .0000, .0000, .0000, .0002, .0000,
    .0000, .0000, .0000, .0000, .0000, .0004,
]


class Node(object):

    def __init__(self):
        rospy.Subscriber("idmind_motors/encoders", Encoders, self.on_encoders)
        self.pub = rospy.Publisher("odom", Odometry, queue_size=10)
        self.left_ticks = 0
        self.right_ticks = 0
        self.px = 0.
        self.py = 0.
        self.pz = 0.
        
    def on_encoders(self, msg):
        self.left_ticks = msg.left
        self.right_ticks = msg.rigth

    def run(self):
        rate = rospy.Rate(20)
        # wait for first odometry readings
        while not self.left_ticks and not self.right_ticks:
            rate.sleep()

        left_ticks = self.left_ticks
        right_ticks = self.right_ticks

        while not rospy.is_shutdown():
            rate.sleep()

            # calculate tick difference
            ld = self.left_ticks - left_ticks 
            rd = self.right_ticks - right_ticks

            # reset ticks
            left_ticks = self.left_ticks
            right_ticks = self.right_ticks

            # calculate individual w
            lw = ld * 2 * math.pi / LEFT_TICKS_PER_TURN * 20
            rw = rd * 2 * math.pi / RIGHT_TICKS_PER_TURN * 20

            # calculate base v and w
            linear = ( rw + lw ) / 2
            angular = ( rw - lw ) / WHEEL_DISTANCE

            # update global position
            self.px += linear * math.cos(self.pz) / 20
            self.py += linear * math.sin(self.pz) / 20
            self.pz += angular / 20

            # build and publish transform
            transform = TransformStamped()
            transform.header.stamp = t
            transform.head.frame_id = "odom"
            transform.child_frame_id = "base_link"
            transform.transform.translation.x = self.px
            transform.transform.translation.y = self.py
            transform.transform.rotation.z = q[2]
            transform.transform.rotation.w = q[3]            
            self.tf_broadcaster.sendTransformMessage(transform)

            # build and publish Odometry message
            q = tf.transformations.quaternion_from_euler(0., 0., self.pz)
            t = rospy.Time.now()
            msg = Odometry()
            msg.header.stamp = t
            msg.header.frame_id = "odom"
            msg.child_frame_id = "base_link"
            msg.pose.pose.position.x = self.px
            msg.pose.pose.position.y = self.py
            msg.pose.pose.orientation.z = q[2]
            msg.pose.pose.orientation.w = q[3]
            msg.pose.covatiance = POSITION_COVARIANCE
            msg.twist.twist.linear.x = linear
            msg.twist.twist.angular.z = angular
            msg.twist.covariance = VELOCITY_COVARIANCE
            self.pub.publish(msg)



if __name__ == "__main__":
    rospy.init_node("idmind_odometry")
    node = Node()
    rospy.loginfo("idmind_odometry: running")
    node.run()
    rospy.spin()