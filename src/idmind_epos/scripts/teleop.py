#!/usr/bin/env python


import math

import rospy
from sensor_msgs.msg import Joy

import handlers


"""

This module implements a ROS Node which converts joystick messages into commands for the robot.

It is configured for a Logitech F710 controller, both in X and D Mode.

Currently supports the following handlers:
    Move -> moves the base, using the left and right knobs
    MoveHead -> turns the head, using the arrow keys (l / r)
    ToggleNav -> toggles navigation commands on / off, using the BACK key
    ToggleFollow -> enables the follow behaviour, using the START key

Extending this node:

    To add new handlers, extend the JoyHandler class in the external handles.py file and register it in 
        the Teleop classe's constructor

    Within this classe's constructor, bind events to changes in the axes or clicks of buttons

    You may bind functions to changes in the axes by calling:
        self.on_change(axis, func), where axis is one of the following:
            "left_knob"
            "right_knob"
            "left_trigger"
            "right_trigger"
            "arrows"
        and func is a function which receives a value parameter.
        the value will be a tuple (x, y) for knobs and arrows, and a single value for the triggers

    You may bind functions to button clicks by calling:
        self.on_click(key, func), where key is one of the following:
            "back"
            "start"
            "x"
            "a"
            "b"
            "y"
            "lb"
            "rb"
        and func is a function which receives no parameters

    

"""


class JoyHandler(object):

    def __init__(self):
        self.handlers = {
            "change": {
                "left_knob": [],
                "right_knob": [],
                "left_trigger": [],
                "right_trigger": [],
                "arrows": []
            },
            "click": {
                "back": [],
                "start": [],
                "x": [],
                "a": [],
                "b": [],
                "y": [],
                "lb": [],
                "rb": []
            }
        }
        self.previous_mapping = {
            "back"          : False,
            "start"         : False,
            "x"             : False,
            "y"             : False,
            "a"             : False,
            "b"             : False,
            "lb"            : False,
            "rb"            : False,
            "ls"            : False,
            "rs"            : False,
            "left_knob"     : (0.0, 0.0),
            "right_knob"    : (0.0, 0.0),
            "left_trigger"  : (0.0, 0.0),
            "right_trigger" : (0.0, 0.0),
            "arrows"        : (0.0, 0.0), 
        }

    def on_click(self, key, handler):
        self.handlers["click"][key].append(handler)

    def on_change(self, axes, handler):
        self.handlers["change"][axes].append(handler)

    def update(self, mapping):
        for axis in ("left_knob", "right_knob", "left_trigger", "right_trigger", "arrows"):
            if self.previous_mapping[axis] != mapping[axis]:
                for handler in self.handlers["change"][axis]:
                    handler(mapping[axis])

        for button in ("back", "start", "x", "a", "b", "y", "lb", "rb", "ls", "rs"):
            if self.previous_mapping[button] and not mapping[button]:
                for handler in self.handlers["click"][button]:
                    handler()

        self.previous_mapping = mapping


class Mapper(object):

    class X(object):
        # axes
        LX = 0
        LY = 1
        LT = 2
        RX = 3
        RY = 4
        RT = 5
        AX = 6
        AY = 7

        # buttons
        A = 0
        B = 1
        X = 2
        Y = 3
        LB = 4
        RB = 5
        BACK = 6
        START = 7
        LS = 9
        RS = 10

        def __init__(self):
            self.lt_initialized = False
            self.rt_initialized = False

        def __call__(self, msg):
            if not self.lt_initialized and msg.axes[self.LT]:
                self.lt_initialized = True
            if not self.rt_initialized and msg.axes[self.RT]:
                self.rt_initialized = True

            mapping = {
                "back"          : msg.buttons[self.BACK  ],
                "start"         : msg.buttons[self.START ],
                "x"             : msg.buttons[self.X     ],
                "y"             : msg.buttons[self.Y     ],
                "a"             : msg.buttons[self.A     ],
                "b"             : msg.buttons[self.B     ],
                "lb"            : msg.buttons[self.LB    ],
                "rb"            : msg.buttons[self.RB    ],
                "ls"            : msg.buttons[self.LS    ],
                "rs"            : msg.buttons[self.LS    ],
                "left_knob"     : (msg.axes[self.LX], msg.axes[self.LY]),
                "right_knob"    : (msg.axes[self.RX], msg.axes[self.RY]),
                "left_stick"    : msg.buttons[self.LS    ],
                "left_trigger"  : msg.axes[self.LT] if self.lt_initialized else 1.,
                "right_trigger" : msg.axes[self.RT] if self.rt_initialized else 1.,               
                "arrows"        : (msg.axes[self.AX], msg.axes[self.AY])
            }    
            return mapping


    class D(object):
        # axes
        LX = 0
        LY = 1
        RX = 2
        RY = 3
        AX = 4
        AY = 5

        # buttons
        X = 0
        A = 1
        B = 2
        Y = 3
        LB = 4
        RB = 5
        LT = 6
        RT = 7
        BACK = 8
        START = 9
        LS = 10
        RS = 11

        def __init__(self):
            pass

        def __call__(self, msg):
            mapping = {
                "back"          : msg.buttons[self.BACK  ],
                "start"         : msg.buttons[self.START ],
                "x"             : msg.buttons[self.X     ],
                "y"             : msg.buttons[self.Y     ],
                "a"             : msg.buttons[self.A     ],
                "b"             : msg.buttons[self.B     ],
                "lb"            : msg.buttons[self.LB    ],
                "rb"            : msg.buttons[self.RB    ],
                "ls"            : msg.buttons[self.LS    ],
                "rs"            : msg.buttons[self.LS    ],
                "left_knob"     : (msg.axes[self.LX], msg.axes[self.LY]),
                "right_knob"    : (msg.axes[self.RX], msg.axes[self.RY]),
                "left_stick"    : msg.buttons[self.LS    ],
                "left_trigger"  : msg.buttons[self.LT    ],
                "right_trigger" : msg.buttons[self.RT    ],               
                "arrows"        : (msg.axes[self.AX], msg.axes[self.AY])
            }    
            return mapping

    def __init__(self):
        self.x_mode = Mapper.X()
        self.d_mode = Mapper.D()
        self.lt_initialized = False
        self.rt_initialized = False

    def __call__(self, msg):
        mode = self.check_mode(msg)
        return mode(msg)

    def check_mode(self, msg):
        return self.x_mode if len(msg.axes) == 8 else self.d_mode


class Teleop(object):
    
    def __init__(self):

        self.mapper = Mapper()
        self.handlers = [
            handlers.Move(),
            handlers.Leds()
        ]
        rospy.Subscriber("/joy", Joy, self.on_joy)

    def on_joy(self, msg):
        mapping = self.mapper(msg)
        for handler in self.handlers:
            handler.update(mapping)

    def run(self):
        rate = rospy.Rate(20)
        while not rospy.is_shutdown():
            for handler in self.handlers:
                handler.run()
            rate.sleep()


if __name__ == '__main__':
    rospy.init_node("teleop")
    node = Teleop()
    rospy.loginfo("teleop: running")
    node.run()
    rospy.spin()
