#include "ros/ros.h"

#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Twist.h"
#include "idmind_msgs/Encoders.h"
#include "std_srvs/SetBool.h"

#include "epos_library/Definitions.h"
#include <string.h>


typedef void* HANDLE;
typedef int BOOL;
typedef unsigned char BYTE;
typedef unsigned int DWORD;
typedef unsigned short WORD;
typedef signed int BOOL;


#ifndef TRUE
    #define TRUE 1
#endif

#ifndef FALSE
    #define FALSE 0
#endif

#ifndef MMC_SUCCESS
	#define MMC_SUCCESS 0
#endif

#ifndef MMC_FAILED
	#define MMC_FAILED 1
#endif

#ifndef PI
    #define PI 3.141592
#endif

#ifndef FRONT_LEFT_WHEEL
    #define FRONT_LEFT_WHEEL 1
#endif

#ifndef FRONT_RIGHT_WHEEL
    #define FRONT_RIGHT_WHEEL 2
#endif

#ifndef REAR_LEFT_WHEEL
    #define REAR_LEFT_WHEEL 4
#endif

#ifndef REAR_RIGHT_WHEEL
    #define REAR_RIGHT_WHEEL 3
#endif

#ifndef FRONT_LEFT_WHEEL_FACTOR
    #define FRONT_LEFT_WHEEL_FACTOR 26.0
#endif

#ifndef FRONT_RIGHT_WHEEL_FACTOR
    #define FRONT_RIGHT_WHEEL_FACTOR -26.0
#endif

#ifndef REAR_LEFT_WHEEL_FACTOR
    #define REAR_LEFT_WHEEL_FACTOR 26.0
#endif

#ifndef REAR_RIGHT_WHEEL_FACTOR
    #define REAR_RIGHT_WHEEL_FACTOR -26.0
#endif

#ifndef WHEEL_RADIUS
    #define WHEEL_RADIUS 0.1015
#endif

#ifndef LOOP_RATE
    #define LOOP_RATE 20
#endif

#ifndef WHEEL_LEFT_TO_RIGHT
    #define WHEEL_LEFT_TO_RIGHT 0.59
#endif

#ifndef WHEEL_FRONT_TO_REAR
    #define WHEEL_FRONT_TO_REAR 0.4622
#endif


HANDLE pBoard = 0;


int flw_rpm = 0;
int frw_rpm = 0;
int rlw_rpm = 0;
int rrw_rpm = 0;

void OnTwist(const geometry_msgs::Twist::ConstPtr& msg)
{

    double vx = msg->linear.x;
    double vy = msg->linear.y;
    double w = msg->angular.z;
    
    double flw = (1. / WHEEL_RADIUS) * (vx - vy - (WHEEL_LEFT_TO_RIGHT / 2 + WHEEL_FRONT_TO_REAR / 2) * w);
    double frw = (1. / WHEEL_RADIUS) * (vx + vy + (WHEEL_LEFT_TO_RIGHT / 2 + WHEEL_FRONT_TO_REAR / 2) * w);
    double rlw = (1. / WHEEL_RADIUS) * (vx + vy - (WHEEL_LEFT_TO_RIGHT / 2 + WHEEL_FRONT_TO_REAR / 2) * w);
    double rrw = (1. / WHEEL_RADIUS) * (vx - vy + (WHEEL_LEFT_TO_RIGHT / 2 + WHEEL_FRONT_TO_REAR / 2) * w);
    
    flw_rpm = 60 / (2 * PI) * flw * FRONT_LEFT_WHEEL_FACTOR;
    frw_rpm = 60 / (2 * PI) * frw * FRONT_RIGHT_WHEEL_FACTOR;
    rlw_rpm = 60 / (2 * PI) * rlw * REAR_LEFT_WHEEL_FACTOR;
    rrw_rpm = 60 / (2 * PI) * rrw * REAR_RIGHT_WHEEL_FACTOR;
}


int OpenDevice()
{
    unsigned int ulErrorCode = 0;

    int lResult = MMC_FAILED;

    // set default parameters
    unsigned short g_usNodeId = 1;
	std::string g_deviceName = "EPOS4"; 
	std::string g_protocolStackName = "MAXON SERIAL V2"; 
	std::string g_interfaceName = "USB"; 
	std::string g_portName = "USB0"; 
	int g_baudrate = 1000000;
    
    char* pDeviceName = new char[255];
	char* pProtocolStackName = new char[255];
	char* pInterfaceName = new char[255];
	char* pPortName = new char[255];

    strcpy(pDeviceName, g_deviceName.c_str());
	strcpy(pProtocolStackName, g_protocolStackName.c_str());
	strcpy(pInterfaceName, g_interfaceName.c_str());
	strcpy(pPortName, g_portName.c_str());

    pBoard = VCS_OpenDevice(pDeviceName, pProtocolStackName, pInterfaceName, pPortName, &ulErrorCode);

    if(pBoard!=0 && ulErrorCode == 0)
	{
		unsigned int lBaudrate = 0;
		unsigned int lTimeout = 0;

		if(VCS_GetProtocolStackSettings(pBoard, &lBaudrate, &lTimeout, &ulErrorCode)!=0)
		{
			if(VCS_SetProtocolStackSettings(pBoard, g_baudrate, lTimeout, &ulErrorCode)!=0)
			{
				if(VCS_GetProtocolStackSettings(pBoard, &lBaudrate, &lTimeout, &ulErrorCode)!=0)
				{
					if(g_baudrate==(int)lBaudrate)
					{
						lResult = MMC_SUCCESS;
					}
				}
			}
		}
	}

    else
    {
        pBoard = 0;
    }

    delete []pDeviceName;
	delete []pProtocolStackName;
	delete []pInterfaceName;
	delete []pPortName;

    return lResult;

}


int CheckDevice()
{
    const WORD maxStrSize = 100;
    char* deviceNameSel = new char[maxStrSize];
    BOOL endOfSelection = FALSE;
    DWORD errorCode = 0;
    return VCS_GetDeviceNameSelection(TRUE, deviceNameSel, maxStrSize, &endOfSelection, &errorCode);
}


int PrepareDevice()
{
    /*
     * Enables the motors
     */ 

    unsigned int ulErrorCode = 0;
    BOOL oIsEnabled = 0;

    VCS_GetEnableState(pBoard, FRONT_LEFT_WHEEL, &oIsEnabled, &ulErrorCode);
    if (!oIsEnabled)
    {
        VCS_SetEnableState(pBoard, FRONT_LEFT_WHEEL, &ulErrorCode);
    }
    
    VCS_GetEnableState(pBoard, FRONT_RIGHT_WHEEL, &oIsEnabled, &ulErrorCode);
    if (!oIsEnabled)
    {
        VCS_SetEnableState(pBoard, FRONT_RIGHT_WHEEL, &ulErrorCode);
    }
    
    VCS_GetEnableState(pBoard, REAR_LEFT_WHEEL, &oIsEnabled, &ulErrorCode);
    if (!oIsEnabled)
    {
        VCS_SetEnableState(pBoard, REAR_LEFT_WHEEL, &ulErrorCode);
    }
    
    VCS_GetEnableState(pBoard, REAR_RIGHT_WHEEL, &oIsEnabled, &ulErrorCode);
    if (!oIsEnabled)
    {
        VCS_SetEnableState(pBoard, REAR_RIGHT_WHEEL, &ulErrorCode);
    }

    /*
     * Sets Velocity Profile mode
     */

    VCS_ActivateProfileVelocityMode(pBoard, FRONT_LEFT_WHEEL, &ulErrorCode);
    VCS_ActivateProfileVelocityMode(pBoard, FRONT_RIGHT_WHEEL, &ulErrorCode);    
    VCS_ActivateProfileVelocityMode(pBoard, REAR_LEFT_WHEEL, &ulErrorCode);
    VCS_ActivateProfileVelocityMode(pBoard, REAR_RIGHT_WHEEL, &ulErrorCode);    
    
    return 0;
}


int CloseDevice()
{
    /*
     * Closes connection to the motors
     */ 

    unsigned int ulErrorCode = 0;
    VCS_CloseDevice(pBoard, &ulErrorCode);

    return 0;
}


int ReadTicks(int* l_frontLeftPos, int* l_frontRightPos, int* l_rearLeftPos, int* l_rearRightPos)
{    
    unsigned int ulErrorCode = 0;
    VCS_GetPositionIs(pBoard, FRONT_LEFT_WHEEL, l_frontLeftPos, &ulErrorCode);
    VCS_GetPositionIs(pBoard, FRONT_RIGHT_WHEEL, l_frontRightPos, &ulErrorCode);
    VCS_GetPositionIs(pBoard, REAR_LEFT_WHEEL, l_rearLeftPos, &ulErrorCode);
    VCS_GetPositionIs(pBoard, REAR_RIGHT_WHEEL, l_rearRightPos, &ulErrorCode);

    return 0;
}


int ReadCurrents(short* l_frontLeftCurr, short* l_frontRightCurr, short* l_rearLeftCurr, short* l_rearRightCurr)
{    
    unsigned int ulErrorCode = 0;
    VCS_GetCurrentIs(pBoard, FRONT_LEFT_WHEEL, l_frontLeftCurr, &ulErrorCode);
    VCS_GetCurrentIs(pBoard, FRONT_RIGHT_WHEEL, l_frontRightCurr, &ulErrorCode);
    VCS_GetCurrentIs(pBoard, REAR_LEFT_WHEEL, l_rearLeftCurr, &ulErrorCode);
    VCS_GetCurrentIs(pBoard, REAR_RIGHT_WHEEL, l_rearRightCurr, &ulErrorCode);

    BYTE nbOfDeviceError;
    unsigned int deviceErrorCode = 0;
    unsigned int errorCode = 0;

    return 0;
}


int main(int argc, char**argv)
{

    OpenDevice();
    PrepareDevice();

    bool connection_lost = false, driver_enabled = true;

    ros::init(argc, argv, "idmind_motors");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("idmind_motors/twist", 1000, OnTwist);
    ros::Publisher pub = n.advertise<idmind_msgs::Encoders>("idmind_motors/encoders", 5);
    ros::Publisher curr_pub = n.advertise<idmind_msgs::Encoders>("idmind_motors/currents", 5);

    ros::ServiceClient enable_epos_driver_srv = n.serviceClient<std_srvs::SetBool>("idmind_sensorsboard/enable_epos");
    ros::ServiceClient take_over_srv = n.serviceClient<std_srvs::SetBool>("idmind_sensorsboard/take_over");

    std_srvs::SetBool req;
    req.request.data = true;
    take_over_srv.call(req);

    ROS_INFO("idmind_motors: running");

    ros::Rate loop_rate(LOOP_RATE);
    while (ros::ok())
    {
        ros::spinOnce();
        loop_rate.sleep();

        int frontLeftPos = 0, frontRightPos = 0, rearLeftPos = 0, rearRightPos = 0;
        short frontLeftCurr = 0, frontRightCurr = 0, rearLeftCurr = 0, rearRightCurr = 0;

        idmind_msgs::Encoders encoders;
        ReadTicks(&frontLeftPos, &frontRightPos, &rearLeftPos, &rearRightPos);
        encoders.frontLeft = frontLeftPos;
        encoders.frontRight = frontRightPos;
        encoders.rearLeft = rearLeftPos;
        encoders.rearRight = rearRightPos;
        pub.publish(encoders);

        idmind_msgs::Encoders currents;
        ReadCurrents(&frontLeftCurr, &frontRightCurr, &rearLeftCurr, &rearRightCurr);
        currents.frontLeft = frontLeftCurr;
        currents.frontRight = frontRightCurr;
        currents.rearLeft = rearLeftCurr;
        currents.rearRight = rearRightCurr;
        curr_pub.publish(currents);

        if (frontLeftCurr == 0 && frontRightCurr == 0 && rearLeftCurr == 0 && rearRightCurr == 0)
        {
            ROS_WARN("connection: NOT OK");
            connection_lost = true;
        }

        else
        {
            connection_lost = false;
            driver_enabled = true;
    
            unsigned int ulErrorCode = 0;
            VCS_MoveWithVelocity(pBoard, FRONT_LEFT_WHEEL, flw_rpm, &ulErrorCode);
            VCS_MoveWithVelocity(pBoard, FRONT_RIGHT_WHEEL, frw_rpm, &ulErrorCode);
            VCS_MoveWithVelocity(pBoard, REAR_LEFT_WHEEL, rlw_rpm, &ulErrorCode);
            VCS_MoveWithVelocity(pBoard, REAR_RIGHT_WHEEL, rrw_rpm, &ulErrorCode);
        }

        if (connection_lost)
        {
            if (driver_enabled) {
                ROS_INFO("disabling driver");
                std_srvs::SetBool req;
                req.request.data = false;
                enable_epos_driver_srv.call(req);
                ROS_INFO("enabling driver");
                req.request.data = true;
                enable_epos_driver_srv.call(req);
                driver_enabled = false;
            }

            ROS_INFO("attempting reconnect");
            CloseDevice();
            OpenDevice();
            PrepareDevice();
        }
    }

    CloseDevice();

    return 0;

}

