# IDMIND Arco

This repository contains ROS drivers for the final platform of the robot ARCO.

These drivers will enable the user to control the velocity of the platform, read IMU and camera feed and interact with the electronics management board to read battery levels, control LEDs and interface with the weighing scale.

Below you will find a description of the packages installed on the robot, and how you can interface with them.

## idmind_epos

This package provides a wrapper for the EPOS4 Command Library for Linux.

The main ros node, src/motors.cpp, will open a connection to the controllers and start publishing encoder readings and current levels. 

The node will enter a reconnect state if the currents are 0. During this state, the rate of publication will drop, and the motors' quickstop will be activated by the sensors board.

Encoders are published as an absolute value, instead of the usual difference from the last read, so the implementation of odometry will need to take overflow into account. From what we understand, the range of encoder readings is the same as an unsigned integer (0 to 65,535).

Additionaly, we have included a teleoperation program configured for a Logitech F710 controller. The controls are:

    start       : toggle control (starts disabled)
    left knob   : linear movement
    right knob  : angular movement
    a           : set the led strip to green
    b           : set the led strip to red
    x           : set the led strip to blue

Base platform dimensions are used in the main node, defined as constants.

### API

        twist [geometry_msgs/Twist]       : set platform speed
        encoders [idmind_msgs/Encoders]   : read encoder values
        currents [idmind_msgs/Encoders]   : read current values


## idmind_sensorsboard

This package provides a ROS interface to the sensors board. 

This board is responsible for managing the epos4 quickstop, the weighing scale's power, reading battery values, undocking and sending colors to the LED strip.

By default, the management board has no power over the quickstop pin. The take over is done via a call to the take_over service. This is done by the motors controller.

The platform currently has a strip of 8 rgb leds. You can set the led colors by using the custom message type. An example, which sets all leds to red:

```
red: [255, 255, 255, 255, 255, 255, 255, 255]

green: [0, 0, 0, 0, 0, 0, 0, 0]

blue: [0, 0, 0, 0, 0, 0, 0, 0]
```
### API

    voltages [idmind_msgs/Voltages]             : Battery levels (voltage and current)
    system_status [idmind_msgs/SystemStatus]    : Charging / power state
    undock [std_srvs/Trigger]                   : Disable charging state
    firmware [std_srvs/Trigger]                 : Query firmware version number
    take_over [std_srvs/SetBool]                : Enables epos4 take over (called by epos4 node)
    enable_epos [std_srvs/SetBool]              : Enables quickstop
    enable_scale [std_srvs/SetBool]             : Enables scale
    leds [idmind_msgs/Leds]                     : Sets led strip color

## idmind_kernscale

This package proves an interface to the weighing scale of the platform.

The scale's power is managed by the sensorsboard.

Using this node, you can query the registered weight, on demand, and reset the tare.

### API

    is_enabled [std_srvs/Trigger]   : Query enable state
    reset_tare [std_srvs/Trigger]   : Resets the tare
    get_weight [idmind_msgs/Weight] : Query the weight

## idmind_msgs

This package contains custom message and service files for the platform.

## idmind_launch

This package contains example launch files, for the platform.

The default launch file will launch interfaces with all hardware, as well as the teleoperation program contained in the idmind_epos package.

## vendor packages

The packages idmind_imu, astra_camera and astra_launch and urg_node have also been installed on the robot.
