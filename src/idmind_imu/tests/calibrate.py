#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Imu
from tf_conversions import transformations as trf


def print_euler(msg):
    q = [msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w]
    print(trf.euler_from_quaternion(q))


rospy.init_node("imu_calibrate")
rospy.Subscriber("/imu", Imu, print_euler)

r = rospy.Rate(20)
while not rospy.is_shutdown():
    r.sleep()