#! /usr/bin/env python

import sys
import rospy
from idmind_arcoboard.msg import Leds


NLEDS = int(sys.argv[1]) if len(sys.argv) > 1 else 10
print "using %s leds" % NLEDS 

colors = {
    "red": Leds([255] * NLEDS, [0] * NLEDS, [0] * NLEDS),
    "green": Leds([0] * NLEDS, [255] * NLEDS, [0] * NLEDS),
    "blue": Leds([0] * NLEDS, [0] * NLEDS, [255] * NLEDS),
    "white": Leds([255] * NLEDS, [255] * NLEDS, [255] * NLEDS),
    "black": Leds([0] * NLEDS, [0] * NLEDS, [0] * NLEDS),
}



if __name__ == "__main__":
    rospy.init_node("test_leds")
    pub = rospy.Publisher("idmind_arcoboard/leds", Leds, queue_size=1)

    print "welcome to the test_leds tools"
    print "write a color or exit"
    shutdown = False
    while not rospy.is_shutdown() and not shutdown:
        rospy.sleep(1.0)
        print "available colors: red, green, blue, white, black"
        color = raw_input()
        if color in ["exit", "e", "E", "EXIT"]:
            shutdown = True
        else:
            c = colors[color.split(" ")[0]] 
            pub.publish(c)