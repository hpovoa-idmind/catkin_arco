#! /usr/bin/env python
# -*- coding: utf-8 -*-

from idmind_serial import IDMindSerial


class InterfaceException(Exception):
    pass


def exception_wrapper(f):
    def wrapped(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception:
            raise InterfaceException()
    return wrapped


class SensorsBoard(IDMindSerial):

    firmware = ""
    serial_ports_open = .0
    motor_battery_voltage = .0
    motor_battery_current = .0
    electronic_battery_voltage = .0
    electronic_battery_current = .0
    cable_power_voltage = .0
    motor_power_voltage = .0
    bumper_no = False
    bumper_so = False
    bumper_se = False
    bumper_ne = False
    # system status
    electronic_battery = False
    motor_battery = False
    cable_power = False
    bumper = False
    enable_to_epos = False
    emergency_stop = False
    motor_button_ON = False
    motor_enable_ON = False


    def __init__(self, address="/dev/idmind-sensorsboard"):

        try: 
            IDMindSerial.__init__(self, address, baudrate=115200, timeout=.5)
        except Exception as e:
            print "Exception raised connecting to Sensors Board"
            self.serial_ports_open = -1
            raise InterfaceException()

    #  SETS

    @exception_wrapper
    def set_number_rgb_leds(self, number):
        return self.command([0x40, int(number)], 4)

    @exception_wrapper
    def set_rgb_leds(self, red, green, blue):
        # tricky one
        combined = zip(red, green, blue)
        flattened = [item for sublist in combined for item in sublist]
        return self.command([0x41] + flattened, 4)

    @exception_wrapper
    def enable_motor_power_stage(self, enable):
        # connect [1] / disconnect [0]  motor batteries
        return self.command([0x42, enable], 4)

    @exception_wrapper
    def enable_electronic_power_stage(self, enable):
        # connect [1] / disconnect [0]  electronic batteries
        return self.command([0x43, enable], 4)

    @exception_wrapper
    def enable_status_lights(self, green=False, yellow=False, red=False):
        b = 1 * red + 2 * yellow + 4 * green
        return self.command([0x44, b], 4)

    @exception_wrapper
    def enable_motor_state(self, board_takeover=False, epo4_drivers=False):
        # enable motor power
        b = board_takeover * 128 + epo4_drivers * 1
        return self.command([0x45, b], 4)

    @exception_wrapper
    def enable_scale(self):
        return self.command([0x46], 4)

    #  GETS

    @exception_wrapper
    def get_voltage_and_current(self):
        response = []
        if not self.command([0x30], 16, response):
            return False
        response = map(ord, response)
        self.electronic_battery_voltage = int(response[0]) * 0x0100 + int(response[1]) * 0x0001
        self.electronic_battery_current = int(response[2]) * 0x0100 + int(response[3]) * 0x0001
        self.cable_power_voltage        = int(response[4]) * 0x0100 + int(response[5]) * 0x0001
        self.motor_battery_voltage      = int(response[6]) * 0x0100 + int(response[7]) * 0x0001
        self.motor_battery_current      = int(response[8]) * 0x0100 + int(response[9]) * 0x0001
        self.motor_power_voltage        = int(response[10]) * 0x0100 + int(response[11]) * 0x0001
        return True

    @exception_wrapper
    def get_bumper_status(self):
        response = []
        if not self.command([0x31], 5, response):
            return False
        response = map(ord, response)
        byte = response[0]
        self.bumper_no = bool(byte & 0x01)   
        self.bumper_so = bool(byte & 0x02) 
        self.bumper_se = bool(byte & 0x04) 
        self.bumper_ne = bool(byte & 0x08)
        return True

    @exception_wrapper
    def get_system_status(self):
        response = []
        if not self.command([0x32], 5, response):
            return False
        response = map(ord, response)
        byte = response[0]
        self.electronic_battery     = bool(byte & 0x01)   
        self.motor_battery          = bool(byte & 0x02)
        self.cable_power            = bool(byte & 0x04) 
        self.bumper                 = bool(byte & 0x08)
        self.enable_to_epos         = bool(byte & 0x10)   
        self.emergency_stop         = bool(byte & 0x20) 
        self.motor_button_ON        = bool(byte & 0x40) 
        self.motor_enable_ON        = bool(byte & 0x80)
        return True

    @exception_wrapper
    def get_firmware_version_number(self):
        response = []
        if not self.command([0x20], 29, response):
            return False
        self.firmware = ""
        for i in response:
            self.firmware += i
        return True

    
        


      
