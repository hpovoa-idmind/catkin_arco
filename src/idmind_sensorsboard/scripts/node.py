#! /usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from idmind_sensorsboard_interface import SensorsBoard, InterfaceException
from idmind_msgs.msg import Bumpers, Leds, SystemStatus, Voltages
from std_srvs.srv import Trigger, SetBool


NLEDS = 8


class Node(object):
    
    def __init__(self):
        self.board = SensorsBoard()

        self.enable_status_lights = False

        self.board.set_number_rgb_leds(NLEDS)
        self.board.get_firmware_version_number()
        self.firmware = self.board.firmware
        self.leds = None
        self.undock = False

        self.motor_state_req = False
        self.motor_state = [False, True]

        self.scale_enabled = False

        self.voltages_pub = rospy.Publisher("idmind_sensorsboard/voltages", Voltages, queue_size=1)
        self.bumpers_pub = rospy.Publisher("idmind_sensorsboard/bumpers", Bumpers, queue_size=1)
        self.system_status_pub = rospy.Publisher("idmind_sensorsboard/system_status", SystemStatus, queue_size=1)

        self.is_scale_enabled_srv = rospy.ServiceProxy("scale/is_enabled", Trigger)

        rospy.Service("idmind_sensorsboard/undock", Trigger, self.on_undock)
        rospy.Service("idmind_sensorsboard/firmware", Trigger, self.on_firmware)
        rospy.Service("idmind_sensorsboard/take_over", SetBool, self.on_take_over)
        rospy.Service("idmind_sensorsboard/enable_epos", SetBool, self.on_enable_epos)
        rospy.Service("idmind_sensorsboard/enable_scale", SetBool, self.on_enable_scale)

        rospy.Subscriber("idmind_sensorsboard/leds", Leds, self.on_leds)

    def write_to_board(self):
        if self.leds != None:
            if self.board.set_rgb_leds(self.leds["red"], self.leds["green"], self.leds["blue"]):
                self.leds = None
        if self.undock:
            success = (
                self.board.enable_motor_power_stage(True) and
                self.board.enable_electronic_power_stage(True) and
                self.board.enable_motor_state(True)
            )
            if success: 
                self.undock = False
        if self.enable_status_lights != None:
            if self.board.enable_status_lights(self.enable_status_lights):
                self.enable_status_lights = None
        if self.motor_state_req:
            if self.board.enable_motor_state(*self.motor_state):
                self.motor_state_req = False

    def read_from_board(self):
        self.board.get_voltage_and_current()
        self.board.get_bumper_status()
        self.board.get_system_status()

        self.voltages = Voltages(
            self.board.electronic_battery_voltage / 100.,
            self.board.electronic_battery_current / 1000.,
            self.board.cable_power_voltage / 100.,
            self.board.motor_battery_voltage / 100.,
            self.board.motor_battery_current / 1000.,
            self.board.motor_power_voltage / 100.,
        )

        self.bumpers = Bumpers(
            self.board.bumper_no,
            self.board.bumper_so,
            self.board.bumper_se,
            self.board.bumper_ne,
        )

        self.system_status = SystemStatus(
            self.board.electronic_battery,
            self.board.motor_battery,
            self.board.cable_power,
            self.board.bumper,
            self.board.enable_to_epos,
            self.board.emergency_stop,
            self.board.motor_button_ON,
            self.board.motor_enable_ON,
        )
        
    def publish(self):
        self.voltages_pub.publish(self.voltages)
        self.bumpers_pub.publish(self.bumpers)
        self.system_status_pub.publish(self.system_status)

    def on_undock(self, _):
        self.undock = True
        while self.undock:
            rospy.sleep(1.0)
        return (True, "robot undocked")

    def on_firmware(self, _):
        return (True, self.firmware)

    def on_leds(self, msg):
        self.leds = {
            "red": msg.red,
            "green": msg.green,
            "blue": msg.blue,
        }

    def on_take_over(self, req):
        self.motor_state[0] = req.data
        self.motor_state_req = True
        while not rospy.is_shutdown() and self.motor_state_req:
            rospy.sleep(1.0)
        msg = 'board took over' if req.data else 'epos took over'
        return (True, msg)

    def on_enable_epos(self, req):
        self.motor_state[1] = req.data
        self.motor_state_req = True
        while not rospy.is_shutdown() and self.motor_state_req:
            rospy.sleep(1.0)
        msg = 'epos driver enabled' if req.data else 'epos driver disabled'
        return (True, msg)

    def on_enable_scale(self, req):
        if req.data is not self.scale_enabled:
            self.board.enable_scale()
            self.scale_enabled = req.data
        msg = "scale enabled" if req.data else "scale disabled"
        return True, msg

    def reconnect(self):
        rospy.sleep(1.0)
        try:
            self.board = SensorsBoard()
        except InterfaceException:
            self.board = None
        rospy.sleep(1.0)

    def run(self):
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            rate.sleep()
            if self.board:
                try:
                    self.write_to_board()
                    self.read_from_board()
                    self.publish()
                except InterfaceException:
                    self.board = None
            else:
                self.reconnect()


if __name__ == '__main__':
    rospy.init_node("idmind_sensorsboard")
    node = Node()
    rospy.loginfo("idmind_sensorsboard: running")
    node.run()
    rospy.spin()
