#! /usr/bin/env python


from serial import Serial
import rospy
from std_srvs.srv import Trigger, Empty
from idmind_msgs.srv import Weight


GET_WEIGHT = 'w'.encode('utf-8')
CALIBRATE_TARE = 't'.encode('utf-8')


class Node(object):

    def __init__(self):
        self.weight_request = False
        self.tare_request = False
        self.board = None
        self.weight = None
        try:
            self.board = Serial(port="/dev/idmind-scale", baudrate=9600, timeout=.5)
        except Exception:
            rospy.logwarn("failed to connect to scale, will retry every second")
        rospy.Service("scale/is_enabled", Trigger, self.on_is_enabled)
        rospy.Service("scale/reset_tare", Trigger, self.on_reset_tare)
        rospy.Service("scale/get_weight", Weight, self.on_get_weight)

    def on_is_enabled(self, _):
        self.weight_request = True
        self.weight = None
        rospy.sleep(rospy.Duration(2.0))
        success = self.weight is not None
        self.weight_request = False
        msg = "scale is enabled" if success else "scale is disabled"
        return success, msg

    def on_reset_tare(self, _):
        self.tare_request = True
        while self.tare_request:
            rospy.sleep(rospy.Duration(1.0))
        return True, "Tare reset"

    def on_get_weight(self, _):
        self.weight_request = True
        while self.weight_request:
            rospy.sleep(rospy.Duration(1.0))
        return True, self.weight        
 
    def get_weight(self):
        self.board.reset_input_buffer()
        self.board.reset_output_buffer()
        self.board.write('w'.encode('utf-8'))
        response = self.board.read(18)
        signal = -1 if response[1] is '-' else 1
        self.weight = signal * float(response[2:12])
        return True

    def reset_tare(self):
        self.board.reset_input_buffer()
        self.board.reset_output_buffer()
        self.board.write('t'.encode('utf-8'))
        return True

    def reconnect(self):
        try:
            self.board = Serial(port="/dev/idmind-scale", baudrate=9600, timeout=.5)
        except Exception:
            return
        rospy.loginfo("successfully reconnected to scale")

    def run(self):
        rate = rospy.Rate(1)
        while not rospy.is_shutdown():
            rate.sleep()
            if not self.board:
                self.reconnect()
            else:
                try:
                    if self.tare_request:
                        if self.reset_tare():
                            self.tare_request = False
                    elif self.weight_request:
                        if self.get_weight():
                            self.weight_request = False
                except Exception as e:
                    self.board = None
                    rospy.logwarn("error sending instruction to scale, will retry every second. reason: %s" % e)


if __name__ == '__main__':
    rospy.init_node("idmind_scale")
    node = Node()
    rospy.loginfo("idmind_scale: running")
    node.run()
    rospy.spin()
